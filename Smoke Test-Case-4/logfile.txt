﻿08/04/2021 10:19:42 *****  Test Case Started *****
08/04/2021 10:19:45 LLW-6960--Test Execution # 01 : Installation of Patient Monitor only installer - fresh installation
      08/04/2021 10:19:45 PM installation process is started
      08/04/2021 10:20:23  PM Installation is in process
      08/04/2021 10:23:07   Installation process is done
08/04/2021 10:23:07 LLW-6960--Test Execution # 01 Status:  Passed

08/04/2021 10:23:09 LLW-6987--Test Execution # 02 : Collection of technical logs from LLEAP
      08/04/2021 10:23:14    LSH has been started
      08/04/2021 10:23:51   Client log collection is in process
      08/04/2021 10:25:30   Client logs are collected successfully
08/04/2021 10:25:30 LLW-6987--Test Execution # 02 Status:  Passed

08/04/2021 10:25:36 LLW-6988	--Test Execution # 03 : Collection of Simulator logs from LLEAP
      08/04/2021 10:25:47   PM has been started
      08/04/2021 10:25:52   Simulator log collection is in process
      08/04/2021 10:26:52    Simulator logs are collected successfully
08/04/2021 10:26:52LLW-6988	--Test Execution # 03 Status:  Passed

08/04/2021 10:27:03 LLW-6979--Test Execution # 04 : Laerdal Scenario Cloud - 1st Sign in and content access
      08/04/2021 10:27:03   LSH has been started
      08/04/2021 10:28:09   Laerdal Scenario Cloud has been signed in
08/04/2021 10:28:59LLW-6979	--Test Execution # 04 Status:  Passed
      08/04/2021 10:29:00   Laerdal Scenario Cloud has been signed out

08/04/2021 10:29:06 LLW-xxxx--Test Execution # 05 : Verification of Tiles
      08/04/2021 10:29:06   LSH has been started
      08/04/2021 10:29:18   Network Selector Tile is working
      08/04/2021 10:29:21   SFNW Tile is working
      08/04/2021 10:29:33   Patient Monitor Selector Tile is working