﻿08/04/2021 09:23:01 *****  Test Case Started *****
08/04/2021 09:23:03 LLW-6960--Test Execution # 01 : Installation of Patient Monitor only installer - fresh installation
      08/04/2021 09:23:03 PM installation process is started
      08/04/2021 09:23:42  PM Installation is in process
      08/04/2021 09:26:37   Installation process is done
08/04/2021 09:26:37 LLW-6960--Test Execution # 01 Status:  Passed

08/04/2021 09:26:39 LLW-6987--Test Execution # 02 : Collection of technical logs from LLEAP
      08/04/2021 09:26:45    LSH has been started
      08/04/2021 09:27:21   Client log collection is in process
      08/04/2021 09:29:20   Client logs are collected successfully
08/04/2021 09:29:20 LLW-6987--Test Execution # 02 Status:  Passed

08/04/2021 09:29:26 LLW-6988	--Test Execution # 03 : Collection of Simulator logs from LLEAP
      08/04/2021 09:29:36   PM has been started
      08/04/2021 09:29:43   Simulator log collection is in process
      08/04/2021 09:30:43    Simulator logs are collected successfully
08/04/2021 09:30:43LLW-6988	--Test Execution # 03 Status:  Passed

08/04/2021 09:30:54 LLW-6979--Test Execution # 04 : Laerdal Scenario Cloud - 1st Sign in and content access
      08/04/2021 09:30:54   LSH has been started
      08/04/2021 09:32:00   Laerdal Scenario Cloud has been signed in
08/04/2021 09:32:50LLW-6979	--Test Execution # 04 Status:  Passed
      08/04/2021 09:32:51   Laerdal Scenario Cloud has been signed out

08/04/2021 09:32:57 LLW-xxxx--Test Execution # 05 : Verification of Tiles
      08/04/2021 09:32:57   LSH has been started
      08/04/2021 09:33:09   Network Selector Tile is working
      08/04/2021 09:33:12   SFNW Tile is working
      08/04/2021 09:33:23   Patient Monitor Selector Tile is working
08/04/2021 09:33:27LLW-xxxx--Test Execution # 05 Status:  Passed

08/04/2021 09:33:27 *****  Test Case Ended *****