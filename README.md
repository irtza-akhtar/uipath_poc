# Smoke Test Automation POC #

This POC is to provide information about the Client Test automation for Smoke testing. The System Under Test Automation is LLEAP version 8.0.0. 

### Repository Distribution ###

There are four modules alongwith test executions for each modules to verify the test results. The modules are listed as:

* LLEAP fresh installation
* LLEAP update
* Patient Monitor fresh installation
* Patient Monitor update

### Setup guidlines ###

* UIpath studio should be installed and run as an administrator mode
	* Load the project .json file from each folder and go to testcase.xaml file and press the debug button on top right corner.
	* For access denied error: Open Windows Task Manager, look for “UiPath User Service” application, select and click “End Task” to kill that process and debug again.
		
* Both installation or update build files should be located in the setup folder in each case folders i.e. (Smoke Test-Case-X\setup).
	* If setup folder is not created before, then create a new folder.
* Build file names should be as:
	* LLEAP: "FullInstructorApplicationSetup"
	* PM: "FullUnifiedPatientMonitorSetup"
* To run the test cases for each new build files, delete the previous installation files from setup folder and paste the new files and rename as written above.

### Further help ###

Contact

* Genie Magbanua (QA Consultant, Laerdal Medical AS)
* Irtza Akhtar (Intern, Laerdal Medical AS)
